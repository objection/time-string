#define _GNU_SOURCE

#include "time-string.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <limits.h>

#define ANSI_RED 		"\x1b[31m"
#define ANSI_GREEN 		"\x1b[32m"
#define ANSI_YELLOW 	"\x1b[33m"
#define ANSI_BLUE 		"\x1b[34m"
#define ANSI_MAGENTA 	"\x1b[35m"
#define ANSI_CYAN 		"\x1b[36m"
#define ANSI_NO_COLOUR 	"\x1b[0m"

enum ts_errno ts_errno;
const char *ts_end_of_parsing;

#define PDIFF(x, y) (x) - (y)
char ts_scratch_buffer[256];
#define x(a, b) b,
char *ts_errno_strs[] = { TS_ERRNO };
#undef x
#define L(x) ((sizeof(x)/sizeof((x)[0])) / ((size_t) \
			(!(sizeof(x) % sizeof((x)[0])))))
#define SWAP(x, y) \
	do { \
		__typeof__ (x) z; \
		z = x; \
		x = y; \
		y = z; \
	} while (0);


const char *maths_operators[] = {
	"+",
	"-",
	"*",
	"/",
	"(",
	")",
};
const char range_operator[] = "..";
const char constant_operator[] = "$";

#define MAKE_STACK_TYPE(type, name) \
			typedef struct stack_##name { \
				type *d; \
				size_t a, t; \
			} stack_##name; \
		stack_##name stack_##name##_create(size_t init_size) { \
			stack_##name s; \
			if (!init_size) \
				init_size = 4; \
			s.d = malloc(sizeof(type) * init_size); \
			if (!s.d) { \
				free(s.d); \
				s.a = -1; \
			} else \
				s.t = 0, s.a = init_size; \
			return s; \
		} \
		int stack_##name##_push(stack_##name *s, type item) { \
			type *tmp; \
			if (s->t >= s->a) { \
				tmp = realloc(s->d, s->a * 2 * sizeof (type)); \
				if (!tmp) \
					return -1;  \
				s->d = tmp; \
				s->a *= 2;  \
			} \
			s->d[s->t++] = item; \
			return s->t; \
		} \
		type stack_##name##_pop (stack_##name *s) { \
			type tmp; \
			if (!s->t) \
				abort (); \
			tmp = s->d[--s->t]; \
			if (s->t * 2 <= s->a && s->a >= 8) { \
				s->a /= 2; \
				s->d = realloc (s->d, s->a * sizeof (type)); \
			} \
			return tmp; \
		} \
		type stack_##name##_peek (stack_##name *s) { \
			return s->d[s->t - 1]; \
		}

// I had to make these macro. I couldn't understand the ISO str lexing
// otherwise. I thought my brains were dribbling out of my ears.
#define SET_LEX_ERROR(error_value, kind) \
	do { \
		kind =  KIND_ERROR; \
		ts_errno = (error_value); \
	} while (0)

// This is called "allow". What it means is if these are set these
// things will be considered the end of the token rather than an error.
enum allow {
	ALLOW_NOTHING = 1 << 0,
	ALLOW_MATHS = 1 << 1,
	ALLOW_RANGE = 1 << 2,
	ALLOW_CONSTANTS = 1 << 3,
};

enum kind {
	KIND_PLUS = '+',
	KIND_MINUS = '-',
	KIND_UNARY_MINUS = 'u',
	KIND_MULTIPLY = '*',
	KIND_DIVIDE = '/',
	KIND_LEFT_BRACKET = '(',
	KIND_RIGHT_BRACKET = ')',
	KIND_DURATION = 128,
	KIND_CONSTANT,
	KIND_ISO,
	KIND_DOT_DOT,
	KIND_NULL_BYTE,
	KIND_ERROR,
	KIND_ERROR_UNRECOGNISED_CONSTANT,
};

enum iso_kind {
	ISO_KIND_NOT_SET,
	ISO_KIND_YEAR_ONLY,
	ISO_KIND_UP_TO_MONTH,
	ISO_KIND_UP_TO_DAY,
	ISO_KIND_UP_TO_HOUR,
	ISO_KIND_UP_TO_MIN,
	ISO_KIND_TIME_ONLY_UP_TO_HOUR,
	ISO_KIND_TIME_ONLY_UP_TO_MIN,
	ISO_KIND_FULL,
	ISO_KIND_TIME_ONLY_FULL,
};

enum parse_flags {
	 parse_flags_expecting_dur = 1 << 0,
};

struct token {
	const char *s, *e;
	enum kind kind;
	enum iso_kind iso_kind;
	struct ts_constant *constant;
	enum parse_flags parse_flags;
};

struct token error_token;

// For printing ts_strerror. You don't want the user to pass
// the string.
//
// Pretty sure this is retarded. It assumes you're calling ts_strerror
// right after, which is reasonable but terrible.
const char *str_s;
const char *str_e;

static bool _ts_strnmatch_in_arr (const char *str, const char **arr,
		size_t narr) {

	for (const char **p = arr; p - arr < narr; p++) {
		if (!strncmp (*p, str, strlen (*p)))
			return 1;
	}
	return 0;
}

static void _ts_back_up_over_spaces (const char **p) {
	if (!isspace (**p)) (*p)--;
	while (isspace (**p)) (*p)--;
	(*p)++;

}


// If allow and something from one of the allow arrays is found,
// return true. If p >= e return true.
//
// The reason for this is a token is a dur or iso str or a "..", for
// ranges, but you don't want to exit without complaining if the user
// isn't asking for a range.
//
// Stage is maybe dodgy. ISO dates are dd-dd-dd, and minuses are -. So
// - is not the end of token during the date portion. You have to put
// in a space. Pass -1 to not bother with stages, ie it's a dur.
static bool _ts_is_legit_terminal (const char *p, const char *e,
		enum allow allow, int stage,
		// A shameful hack. This lib needs to be redone.
		bool t_is_terminal) {
	if (p >= e ||
			(allow & ALLOW_MATHS && _ts_strnmatch_in_arr (p, maths_operators,
				L (maths_operators))) ||
			(allow & ALLOW_RANGE && !strncmp
			 (p, range_operator, L (range_operator) - 1)) ||

			(allow & ALLOW_CONSTANTS && *p == '$')

			// This is for, eg, 1dT17. It's mess.
			|| (t_is_terminal && *p == 'T')) {

		if (stage != -1 && stage < 3 && *p == '-')
			return 0;
		return 1;
	}
	return 0;
}

bool ts_is_leap_year (struct ts_time *time) {
	size_t year = time->tm.tm_year + 1900;
	if ((year % 4 == 0) && (year % 100 != 0))
		return 1;
	else if ((year % 100 == 0) && (year % 400 == 0))
		return 1;
	else if (year % 400 == 0)
		return 1;
	else return 0;
}

time_t ts_secs_from_months (int months, struct ts_time *from_time) {
	// Could put this in an array or something but a switch is faster, I think

	ssize_t r = 0;
	int month = from_time->tm.tm_mon;
	unsigned absolute_months = abs (months);
	bool is_positive = months < 0? 0: 1;
	for (size_t i = 0;
			i < absolute_months;
			i++, month = is_positive
						? (month >= 11? 0: month + 1)
						: (month <= 0? 11: month - 1)) {
		switch (month) {
			case 0: r += TS_SECS_FROM_DAYS (TS_MDAY_JAN); break;
			case 1:
					if (ts_is_leap_year (from_time))
						r += TS_SECS_FROM_DAYS (TS_MDAY_FEB_LEAP_YEAR);
					else
						r += TS_SECS_FROM_DAYS (TS_MDAY_FEB_NON_LEAP_YEAR);
					break;
			case 2: r += TS_SECS_FROM_DAYS (TS_MDAY_MAR); break;
			case 3: r += TS_SECS_FROM_DAYS (TS_MDAY_APR); break;
			case 4: r += TS_SECS_FROM_DAYS (TS_MDAY_MAY); break;
			case 5: r += TS_SECS_FROM_DAYS (TS_MDAY_JUN); break;
			case 6: r += TS_SECS_FROM_DAYS (TS_MDAY_JUL); break;
			case 7: r += TS_SECS_FROM_DAYS (TS_MDAY_AUG); break;
			case 8: r += TS_SECS_FROM_DAYS (TS_MDAY_SEP); break;
			case 9: r += TS_SECS_FROM_DAYS (TS_MDAY_OCT); break;
			case 10: r += TS_SECS_FROM_DAYS (TS_MDAY_NOV); break;
			case 11: r += TS_SECS_FROM_DAYS (TS_MDAY_DEC); break;
			default: assert (! "\
Big error. Months in tm shouldn't exceed 11\n");
					 break;
		}
	}
	return is_positive? r: -r;
}

#if 0
bool _is_all_white (const char *s, const char *e) {
	if (!e)
		e = strchr (s, '\0');
	for (; s < e; s++) {
		if (!isspace (*s)) {
			return false;
		}
	}
	return true;
}
#endif

// If n2, gets n2 chars, else n1.
// Takes a token because it increments t->e if fails, sets t->kind.
// Ignores t->s because t->s isn't necessarily the start of the digits.
// This is only for ISO times
static int _find_end_of_digits (int *accepted_ndigits,
		size_t naccepted_ndigits,
		const char *start_of_digits,
		struct token *t, const char *e) {

	if (!isdigit (*t->e)) {
		SET_LEX_ERROR (ts_errno_lex_iso_expected_digit, t->kind);
		return 1;
	}
	while (t->e != e && isdigit (*t->e))
		t->e++;
	bool match = 0;
	for (int *p = accepted_ndigits; p - accepted_ndigits <
			naccepted_ndigits; p++) {
		if (t->e - start_of_digits == *p) {
			match = 1;
			break;
		}
	}
	if (!match)
		return 1;
	return 0;
}

static inline void _expect_char_move_ptr (char the_char, struct token *t,
		enum ts_errno errno_if_fail) {
	if (*t->e != the_char) {
		SET_LEX_ERROR (errno_if_fail, t->kind);
	}
	t->e++;
}

static void _ts_lex_dur (struct token *t, const char *e,
		enum allow allow) {
	t->kind = KIND_DURATION;
	do {
		while (isdigit (*t->e))
			t->e++;
		if (!*t->e || t->e == e) {
			SET_LEX_ERROR (ts_errno_lex_dur_missing_unit, t->kind);
			return;
		}
		if (!strchr (DURATION_CHRS, *t->e)) {
			SET_LEX_ERROR (ts_errno_lex_dur_expected_dur_chrs,
					t->kind);
			return;
		}
		t->e++;
		while (isspace (*t->e))
			t->e++;
		if (_ts_is_legit_terminal (t->e, e, allow, -1, 1))
			return;
	} while (1);
}

// Assumes t->e == t->s + 1
static void _ts_lex_iso (struct token *t, const char *e,
		enum allow allow) {

#define $return_if_terminal(_kind) \
	do { \
		if (_ts_is_legit_terminal (t->e, e, allow, stage++, 0)) { \
			t->iso_kind = (_kind); \
			return; \
		} \
	} while (0)
	// Years
	const char *start_of_digits = t->s;
	int stage = 0;

	t->kind = KIND_ISO;
	bool time_only = 0;
	if (*t->s == 'T' || *t->s == 't') {
		time_only = 1;
		// It's a goto.
		goto time_only;
	}

	// FIXME (one day (never)): this is a pretty bad function full of
	// repetition. Use a macro.

	if (_find_end_of_digits ((int []) {2, 4}, 2, start_of_digits, t, e))
		SET_LEX_ERROR (ts_errno_lex_iso_wrong_number_of_year_digits, t->kind);
	if (t->kind != KIND_ISO)
		return;
	$return_if_terminal (ISO_KIND_YEAR_ONLY);
	_expect_char_move_ptr ('-', t,
			ts_errno_lex_iso_expected_hyphen);

	// Months
	start_of_digits = t->e;
	if (_find_end_of_digits ((int []) {1, 2}, 2, start_of_digits, t, e))
		SET_LEX_ERROR (ts_errno_lex_iso_wrong_number_of_month_digits,
				t->kind);
	if (t->kind != KIND_ISO)
		return;
	$return_if_terminal (ISO_KIND_UP_TO_MONTH);
	_expect_char_move_ptr ('-', t, ts_errno_lex_iso_expected_hyphen);


	// Days
	start_of_digits = t->e;
	if (_find_end_of_digits ((int []) {1, 2}, 2,
				start_of_digits, t, e))
		SET_LEX_ERROR (ts_errno_lex_iso_wrong_number_of_day_digits, t->kind);
	if (t->kind != KIND_ISO)
		return;
	$return_if_terminal (ISO_KIND_UP_TO_DAY);

	if (*t->e != ' ' && (*t->e != 'T' || *t->e == 't')) {
		SET_LEX_ERROR (ts_errno_lex_iso_expected_T, t->kind);
		return;
	}
	t->e++;
	if (_ts_is_legit_terminal (t->e, e, allow, stage++, 0)) {
		t->iso_kind = ISO_KIND_UP_TO_DAY;
		return;
	}

time_only:

	// Hours
	start_of_digits = t->e;
	if (_find_end_of_digits ((int []) {1, 2}, 2, start_of_digits, t, e))
		SET_LEX_ERROR (ts_errno_lex_iso_wrong_number_of_hour_digits, t->kind);

	if (t->kind != KIND_ISO)
		return;

	$return_if_terminal (time_only
			? ISO_KIND_TIME_ONLY_UP_TO_HOUR
			: ISO_KIND_UP_TO_HOUR);
	_expect_char_move_ptr (':', t,
			ts_errno_lex_iso_expected_colon);

	// Minutes
	start_of_digits = t->e;
	if (_find_end_of_digits ((int []) {1, 2}, 2, start_of_digits, t, e))
		SET_LEX_ERROR (ts_errno_lex_iso_wrong_number_of_minute_digits, t->kind);
	if (t->kind != KIND_ISO)
		return;
	$return_if_terminal (time_only
			? ISO_KIND_TIME_ONLY_UP_TO_MIN
			: ISO_KIND_UP_TO_MIN);
	_expect_char_move_ptr (':', t,
			ts_errno_lex_iso_expected_colon);

	// Seconds
	start_of_digits = t->e;
	if (_find_end_of_digits ((int []) {1, 2}, 2, start_of_digits, t, e))
		SET_LEX_ERROR (ts_errno_lex_iso_wrong_number_of_second_digits, t->kind);
	if (!_ts_is_legit_terminal (t->e, e, allow, stage++, 0))
		SET_LEX_ERROR (ts_errno_lex_iso_expected_0, t->kind);
	if (time_only)
		t->iso_kind = ISO_KIND_TIME_ONLY_FULL;
	else
		t->iso_kind = ISO_KIND_FULL;
}

// This isn't really a get_token function. It should be returning NUMBER, etc, not
// ISO.
static struct token ts_get_token (const char *p, const char *e,
		enum allow allow, struct ts_constant *constants, size_t nconstants) {

	ts_errno = ts_errno_no_error;
	struct token r = {.s = p};
	while (isspace (*r.s))
		r.s++;
	if (!*r.s) {
		ts_end_of_parsing = r.s;
		r.kind = KIND_NULL_BYTE;
		return r;
	}
	r.e = r.s + 1;
	switch (*r.s) {
		case '(':
		case ')':
		case '+':
		case '*':
		case '/':
			if (!(allow & ALLOW_MATHS))
				r.kind = KIND_ERROR;
			else r.kind = *r.s;
			break;
		case '-':

			// If you're doing sums, - is a minus or unary minus, so a seperate
			// token. If you're not, it's parsed as a negative dur.

			if ((allow & ALLOW_MATHS))
				r.kind = *r.s;
			else
				_ts_lex_dur (&r, e, allow);
			break;
		case '.':
			if (!(allow & ALLOW_RANGE))
				r.kind = KIND_ERROR;
			else if (*r.e == '.') {
				const char *tmp = r.e + 1;
				while (isspace (*tmp))
					tmp++;
				if (!isdigit (*tmp) && *tmp != '-') {
					if (!(allow & ALLOW_CONSTANTS && *tmp == '$')) {
						SET_LEX_ERROR (ts_errno_lex_range_dot_dot_should_be_digit_after,
								r.kind);
						break;
					}
				}
				r.kind = KIND_DOT_DOT;
				r.e++;
			} else
				SET_LEX_ERROR (ts_errno_lex_expected_dot_dot, r.kind);
			break;
		case 'T':
		case 't':
		case '0' ... '9': {
				const char *tmp = r.e;
				while (isdigit (*tmp))
					tmp++;
				if (tmp < e && strchr (DURATION_CHRS, *tmp))
					_ts_lex_dur (&r, e, allow);
				else
					_ts_lex_iso (&r, e, allow);
			}
			break;
		case '$':
			if (!(allow & ALLOW_CONSTANTS))
				SET_LEX_ERROR (ts_errno_bad_char, r.kind);
			else {
				bool match = 0;
				if (!constants) {
					SET_LEX_ERROR (ts_errno_lex_passed_NULL_constants,
							r.kind);
					break;
				}
				if (!nconstants) {
					SET_LEX_ERROR (ts_errno_lex_nconstants_is_0,
							r.kind);
					break;
				}
				struct ts_constant *p = constants;
				unsigned len = 0;
				for (; p - constants < nconstants; p++) {
					if (!p->name)
						continue;
					if (!strncmp (p->name, r.s + 1, len = strlen
								(p->name))) {
						match = 1;
						break;
					}
				}
				if (match) {
					r.kind = KIND_CONSTANT;
					r.e = r.s + 1 + len;
					r.constant = p;
				} else {
					while (!_ts_is_legit_terminal (r.e, e, allow, -1, 0))
						r.e++;
					r.kind = KIND_ERROR_UNRECOGNISED_CONSTANT;
				}
			}
			break;
		default:
			r.kind = KIND_ERROR;
			ts_errno = ts_errno_lex_str_isnt_dur_or_iso;
			break;
	}

	if (r.kind == KIND_ERROR || r.kind == KIND_ERROR_UNRECOGNISED_CONSTANT)
		error_token = r;
	ts_end_of_parsing = r.s;
	return r;
}

// Return a bool; ts_errno is set to show why it failed.
bool ts_tm_is_valid (struct tm *tm) {
	if (tm->tm_mon < 0) {
		ts_errno = ts_errno_parse_iso_month_not_plusequals_zero;
		return 0;
	}
	if (tm->tm_mon > 11) {
		ts_errno = ts_errno_parse_iso_too_big_mon;
		return 0;
	}
	if (tm->tm_mday < 1) {
		ts_errno = ts_errno_parse_iso_mday_not_plusequals_one;
		return 0;
	}
	int feb_days = ts_is_leap_year (&(struct ts_time) {.tm = *tm})
		? TS_MDAY_FEB_LEAP_YEAR
		: TS_MDAY_FEB_NON_LEAP_YEAR;
	if (tm->tm_mday > 28
			&&
				((tm->tm_mon == 0 && tm->tm_mday > TS_MDAY_JAN)
				|| (tm->tm_mon == 1 && tm->tm_mday > feb_days)
				|| (tm->tm_mon == 2 && tm->tm_mday > TS_MDAY_MAR)
				|| (tm->tm_mon == 3 && tm->tm_mday > TS_MDAY_APR)
				|| (tm->tm_mon == 4 && tm->tm_mday > TS_MDAY_MAY)
				|| (tm->tm_mon == 5 && tm->tm_mday > TS_MDAY_JUN)
				|| (tm->tm_mon == 6 && tm->tm_mday > TS_MDAY_JUL)
				|| (tm->tm_mon == 7 && tm->tm_mday > TS_MDAY_AUG)
				|| (tm->tm_mon == 8 && tm->tm_mday > TS_MDAY_SEP)
				|| (tm->tm_mon == 9 && tm->tm_mday > TS_MDAY_OCT)
				|| (tm->tm_mon == 10 && tm->tm_mday > TS_MDAY_NOV)
				|| (tm->tm_mon == 11 && tm->tm_mday > TS_MDAY_DEC))) {
		ts_errno = ts_errno_parse_iso_too_big_mday;
		return 0;
	}
	if (tm->tm_hour < 0) {
		ts_errno = ts_errno_parse_iso_hour_not_plusequals_zero;
		return 0;
	}
	if (tm->tm_hour > 23) {
		ts_errno = ts_errno_parse_iso_too_big_hour;
		return 0;
	}
	if (tm->tm_min < 0) {
		ts_errno = ts_errno_parse_iso_min_not_plusequals_zero;
	}
	if (tm->tm_min > 59) {
		ts_errno = ts_errno_parse_iso_too_big_min;
		return 0;
	}
	if (tm->tm_sec < 0) {
		ts_errno = ts_errno_parse_iso_sec_not_plusequals_zero;
	}
	if (tm->tm_sec > 59) {
		ts_errno = ts_errno_parse_iso_too_big_sec;
		return 0;
	}
	return 1;
}


// This function is could be not static and could take a start and an
// end so you can use it by itself if you know you have a an ISO str.
// As usual with this library this function just returns r, it doesn't
// return -1 or 0. Actually, it'll probably return zero since it's
// set to that at the start. The point is, check ts_errno.

// FIXME: this function is pretty pointless. If you give strptime a time like
// "21:09:09", it'll give you a tm with the hours, minutes and seconds zeroed
// out. That's what this function was trying to do. So, really, this could be
// replaced with a call to strptime.
static struct ts_time _time_from_iso_8601_str (struct token *t,
		struct ts_time *now) {
	struct ts_time r = {.tm = {.tm_mday = 1, .tm_isdst = -1}};
	// mday goes from 1 to n
	memset (ts_scratch_buffer, 0, sizeof ts_scratch_buffer);
	sprintf (ts_scratch_buffer, "%.*s", (int) (t->e - t->s), t->s);
	// Turn eg "2019-00-00 00:00:00" into "2019-00-00T00:00:00" for strptime.
	char *space = 0;
	if ((space = strchr (ts_scratch_buffer, ' ')))
		*space = 'T';

	char *p = strchr (ts_scratch_buffer, '-');
	if (!p)
		p = strchr (ts_scratch_buffer, '\0'); // for if it's just eg "1900"
	bool is_four_digit_year = p - ts_scratch_buffer == 4 ? 1 : 0;

	// Using scanf because strftime just returns 0 if something goes
	// wrong. Can't tell if it's because you entered 0 for a month or
	// 500.
	int sscanf_ret = 0;
	int expected_sscanf_ret = 0;
	// Quick apology for this awful code full of clags.
	bool full_year = 0;
	switch (t->iso_kind) {
		case ISO_KIND_YEAR_ONLY:
			expected_sscanf_ret = 1;
			sscanf_ret = sscanf (ts_scratch_buffer, "%d", &r.tm.tm_year);
			full_year = 1;
			break;
		case ISO_KIND_UP_TO_MONTH:
			expected_sscanf_ret = 2;
			sscanf_ret = sscanf (ts_scratch_buffer, "%d-%d",
					&r.tm.tm_year, &r.tm.tm_mon);
			full_year = 1;
			break;
		case ISO_KIND_UP_TO_DAY:
			expected_sscanf_ret = 3;
			sscanf_ret = sscanf (ts_scratch_buffer, "%d-%d-%d",
					&r.tm.tm_year, &r.tm.tm_mon, &r.tm.tm_mday);
			full_year = 1;
			break;
		case ISO_KIND_UP_TO_HOUR:
			expected_sscanf_ret = 4;
			sscanf_ret = sscanf (ts_scratch_buffer, "%d-%d-%dT%d",
					&r.tm.tm_year, &r.tm.tm_mon, &r.tm.tm_mday,
					&r.tm.tm_hour);
			full_year = 1;
			break;
		case ISO_KIND_UP_TO_MIN:
			expected_sscanf_ret = 5;
			sscanf_ret = sscanf (ts_scratch_buffer, "%d-%d-%dT%d:%d",
					&r.tm.tm_year, &r.tm.tm_mon, &r.tm.tm_mday,
					&r.tm.tm_hour, &r.tm.tm_min);
			full_year = 1;
			break;
		case ISO_KIND_FULL:
			expected_sscanf_ret = 6;
			sscanf_ret = sscanf (ts_scratch_buffer, "%d-%d-%dT%d:%d:%d",
					&r.tm.tm_year, &r.tm.tm_mon, &r.tm.tm_mday,
					&r.tm.tm_hour, &r.tm.tm_min, &r.tm.tm_sec);
			full_year = 1;
			break;
		case ISO_KIND_TIME_ONLY_UP_TO_HOUR:
			// This and the next case are used in these situations:
			// 		T10
			// 		1dT10
			//
			// In the first case, you're going to end up with "today at 10".
			//
			// In the second, this value is used, but the hours, minutes and
			// year are overwritten. This happens in _ts_get_time,
			// case KIND_DURATION.
			expected_sscanf_ret = 1;
			r.tm = now->tm;
			r.tm.tm_min = r.tm.tm_sec = 0;
			sscanf_ret = sscanf (ts_scratch_buffer, "T%d", &r.tm.tm_hour);
			break;
		case ISO_KIND_TIME_ONLY_UP_TO_MIN:
			expected_sscanf_ret = 2;
			r.tm = now->tm;
			r.tm.tm_sec = 0;
			sscanf_ret = sscanf (ts_scratch_buffer, "T%d:%d", &r.tm.tm_hour,
					&r.tm.tm_min);
			break;
		case ISO_KIND_TIME_ONLY_FULL:
			expected_sscanf_ret = 3;
			r.tm = now->tm;
			sscanf_ret = sscanf (ts_scratch_buffer, "T%d:%d:%d",
					&r.tm.tm_hour, &r.tm.tm_min, &r.tm.tm_sec);
			break;
		default:
		assert (! "Impossible if going through the lexer, right?");
	}
	if (sscanf_ret != expected_sscanf_ret)
		assert (! "sscanf_ret isn't expected_sscanf_ret");
	if (sscanf_ret == EOF)
		assert (! "This shouldn't be allowed");

	if (full_year) {
		if (is_four_digit_year)
			r.tm.tm_year -= 1900;
		else {
			r.tm.tm_year += 2000;
			r.tm.tm_year -= 1900;
		}
	}

	// The user has entered 0 for the month
	if (t->iso_kind != ISO_KIND_YEAR_ONLY
			&& t->iso_kind != ISO_KIND_TIME_ONLY_UP_TO_HOUR
			&& t->iso_kind != ISO_KIND_TIME_ONLY_UP_TO_MIN
			&& t->iso_kind != ISO_KIND_TIME_ONLY_FULL) {
		if (r.tm.tm_mon == 0) {
			ts_errno = ts_errno_parse_iso_mday_not_plusequals_one;
			return r;
		}
		r.tm.tm_mon--;
	}

	if (!ts_tm_is_valid (&r.tm))
		return r;
	r.ts = mktime (&r.tm);
	return r;
}



/*
 * This function is could be not static and could take a start and an
 * end so you can use it by itself if you know you have a megastr. But
 * I don't want that; it complicates things. It doesn't check token is
 * valid.
 * It shouldn't be able to fail because it's already been lexed.
 * Returns -1 if error and sets ts_errno.
 * Takes from_tm because if M (months) you need to know where
 * to count from.
 */
static struct ts_dur _dur_from_dur_str (struct token *t) {
	// Count up with this add or sub now_secs at the end based on
	// whether there's a '-' at the start.
	struct ts_dur r = {};
	const char *p = t->s;

	long int num;
	bool is_minus = *p == '-' ? 1 : 0;
	if (is_minus)
		p++;
	for (; p < t->e; p++) {
		char *end_of_digits;
		while (isspace (*p))
			p++;
		if (p >= t->e)
			break;
		if (isdigit (*p)) {
			num = strtol (p, &end_of_digits, 0);
			if (num == LONG_MAX) {
				ts_errno = ts_errno_parse_dur_too_big_digit;
				// Just return r. Don't set to -1 or something.
				// ts_errno is how you check.
				return r;
			}
			p = end_of_digits;
			switch (*p) {
				case 'y':
					r.secs += TS_SECS_FROM_YEARS (num);
					break;
				case 'w':
					r.secs += TS_SECS_FROM_WEEKS (num);
					break;
				case 'd':
					r.secs += TS_SECS_FROM_DAYS (num);
					break;
				case 'h':
					r.secs += TS_SECS_FROM_HOURS (num);
					break;
				case 'm':
					r.secs += TS_SECS_FROM_MINS (num);
					break;
				case 's':
					r.secs += num;
					break;
				case 'M':
					r.months += num;
					break;
				default:
					assert (! "\
This should be impossible; it should be a valid token\n");
					break;
			}
		} else {
			ts_errno = ts_errno_parse_dur_expected_digit;
			return r;
		}
	}
	if (is_minus) {
		r.secs = -r.secs;
		r.months = -r.months;
	}

	return r;
}

struct ts_dur ts_dur_from_dur_str (const char *s,
		const char *e) {
	struct ts_dur r = {};
	// Run it through this so to check for errors.
	str_s = s;
	if (!e)
		e = strchr (s, '\0');
	str_e = e;
	struct token t = ts_get_token (s, e, ALLOW_NOTHING, 0, 0);
	if (t.kind == KIND_DURATION)
		r = _dur_from_dur_str (&t);
	// shouldn't need to deal with error, should have been set
	return r;
}

// Takes a bool reorder flags. You might not want to reorder because the
// ts_secs_from_dur below is calculated from now, so it's not necessarily
// accurate.
struct ts_dur_range ts_dur_range_from_dur_str (const char *s,
		const char *e, bool reorder) {

	struct ts_dur_range r = {};
	// Run it through this so to check for errors.
	if (!e)
		e = strchr (s, '\0');
	struct token t = ts_get_token (s, e, ALLOW_RANGE, 0, 0);
	if (t.kind != KIND_DURATION || ts_errno)
		return r;

	r.s = _dur_from_dur_str (&t);
	t = ts_get_token (t.e, e, ALLOW_RANGE, 0, 0);
	if (t.kind != KIND_DOT_DOT || ts_errno)
		return r;

	t = ts_get_token (t.e, e, ALLOW_RANGE, 0, 0);
	if (t.kind != KIND_DURATION || ts_errno)
		return r;

	r.e = _dur_from_dur_str (&t);
	if (reorder) {
		struct ts_time _time = ts_time_from_ts (time (0));

		// This is not necessarily accurate because of the different lengths
		// of months but it's fine, I think.
		if (ts_secs_from_dur (r.s, &_time)
				> ts_secs_from_dur (r.e, &_time)) {
			SWAP (r.s, r.e);
		}
	}

	// shouldn't need to deal with error, should have been set
	return r;
}

static struct ts_time _ts_get_time (const char *e, struct token *t, struct ts_time *now,
		enum ts_match match) {
	struct ts_time r = {};
	switch (t->kind) {
		case KIND_ISO:
			if (match == TS_MATCH_ONLY_DURATION)
				ts_errno = ts_errno_passed_match_only_dur_but_str_is_iso;
			else
				r = _time_from_iso_8601_str (t, now);
			break;
		case KIND_DURATION:
			if (match == TS_MATCH_ONLY_ISO) {
				if (*t->e != 'T') {
					ts_errno = ts_errno_passed_match_only_iso_but_str_is_dur;
					break;
				}
			}
			struct ts_dur dur = _dur_from_dur_str (t);
			if (*t->e == 'T') {
				*t = ts_get_token (t->e, e, 0, 0, 0);
				struct ts_time time = _ts_get_time (e, t, now, TS_MATCH_ONLY_ISO);
				r = ts_time_from_ts
					(now->ts + ts_secs_from_dur (dur, &time));
				r.tm.tm_hour = time.tm.tm_hour;
				r.tm.tm_min = time.tm.tm_min;
				r.tm.tm_sec = time.tm.tm_sec;
				r.ts = mktime (&r.tm);
			} else {

				r.ts = now->ts + ts_secs_from_months
					(dur.months, now) + dur.secs;

				localtime_r (&r.ts, &r.tm);
			}
			break;
		case KIND_DOT_DOT:
		case KIND_ERROR: // fall-through
		default:
			break;
	}
	return r;
}

// Parses eg 19-09-09..19-10-10 into a struct ts_ts_range AKA two
// tss.
// It'll also deal with 19-09-09..2d. 2d is interpreted relative to
// now_ts. Just returns if something goes wrong because ts_errno is set
// by get_token and _ts_get_ts. This means it doesn't tell if the
// problem is with the first date or the second. I could add another error
// value or an int with the token number or something else. I certainly won't
// right now. Swaps the two values at the end if s is larger than e.

struct ts_time_range ts_time_range_from_str (
		const char *s, // Start of parsing.
		const char *e, // End of parsing. If 0, parse until '\0.
		struct ts_time *now,

		enum ts_match match) { // Specify what you want to parse.
	struct ts_time_range r = {};
	str_s = s;

	if (!e)
		e = strchr (s, '\0');
	if (isspace (*(e - 1))) {
		e--;
		while (isspace (*e))
			e--;
		e++;
	}
	str_e = e;

	struct token t = ts_get_token (s, e, ALLOW_RANGE, 0, 0);
	if (t.kind != KIND_ISO && t.kind != KIND_DURATION)
		return r; // ts_errno is set

	r.s = _ts_get_time (e, &t, now, match);
	t = ts_get_token (t.e, e, ALLOW_RANGE, 0, 0);
	if (t.kind != KIND_DOT_DOT) {
		ts_errno = ts_errno_range_expected_dot_dot;
		return r;
	}
	t = ts_get_token (t.e, e, ALLOW_NOTHING, 0, 0);
	if (t.kind != KIND_ISO && t.kind != KIND_DURATION)
		return r; // ts_errno is set

	r.e = _ts_get_time (e, &t, now, match);
	if (r.s.ts > r.e.ts) {
		SWAP (r.s.ts, r.e.ts);
		SWAP (r.s.tm, r.e.tm);
	}

	return r;

}

struct ts_ts_range ts_ts_range_from_str (
		const char *s, // Start of parsing.
		const char *e, // End of parsing. If 0, parse until '\0.
		struct ts_time *now,
		enum ts_match match) { // Specify what you want to parse.
	struct ts_time_range range = ts_time_range_from_str (s, e, now, match);
	return (struct ts_ts_range) {.s = range.s.ts, .e = range.e.ts};
}

enum elem_type {
	ELEM_TYPE_NOTHING,
	ELEM_TYPE_TIME,
	ELEM_TYPE_DURATION,
	ELEM_TYPE_OPERATOR,
};

struct elem {
	union {
		struct ts_time time;
		char operator;
		struct ts_dur dur;
	};
	enum elem_type type;
};

int _ts_get_precedence (char operator) {
	switch (operator) {
		case '(':
		case ')':
			return 1;
			break;
		case '+':
		case '-':
			return 2;
		case 'u':
			return 3;
		case '*':
		case '/':
			return 4;
		default:
			assert (! "This char shouldn't have been passed to this");
			break;
	}
	return 0;
}

MAKE_STACK_TYPE (struct elem, elem);
MAKE_STACK_TYPE (time_t, time_t);

static void _deal_with_unary_and_first (struct elem *elem,
		struct stack_elem *operators, bool expect_first, struct ts_time *now) {
	if (operators->t && stack_elem_peek (operators).operator == 'u') {
		stack_elem_pop (operators);
		elem->dur.secs =  -elem->dur.secs;
		elem->dur.months =  -elem->dur.months;
	}
	if (expect_first) {
		elem->time = ts_time_from_ts (now->ts +
				ts_secs_from_dur (elem->dur, now));
		elem->type = ELEM_TYPE_TIME;
	}
}

// This turns eg 2 + 2 into 2 2 +. I'm mostly sure you call this postfix and
// RPN. It also fills *operators. Perhaps it should return a struct with both
// because they are both important results.
// It kludges precedence because before only unary minus, no power or whatever.
//
// It takes a struct token * for ts_maths_time_range_from_str's sake.
// Needs to have {.e} point to the start of what you're parsing.

struct stack_elem _ts_get_postfix (struct token *t, const char *e,
		struct stack_elem *operators, struct ts_time *now,
		struct ts_constant *constants, size_t
		nconstants, enum allow allow, bool dry_run) {

	// Obviously you have to ALLOW_MATHS.
	// You might need to ALLOW_CONSTANTS and ALLOW_RANGE.
	allow |= ALLOW_MATHS;
	struct stack_elem r = stack_elem_create (8);
	struct elem elem = {};

	// These bools are ugly. I should just use a last_token and then you
	// wouldn't notice the ugliness. I won't.

	bool expect_unary = 1;
	bool expect_first = 1; // So you can see if the a digits's after a '('
	bool did_unary = 0;

	// Actually, here is a last_token. It's for functions that might use
	// the struct token * after, and might not expect the token to be
	// past the infix expression. I'm talking about
	// ts_maths_time_range_from_str

	struct token last_token = {};

	while ((*t = ts_get_token (t->e, e, allow,
					constants, nconstants)).kind != KIND_NULL_BYTE) {
		if (t->kind == '-' && expect_unary)
			t->kind = 'u';

		switch (t->kind) {
			case 'u':
				elem = (struct elem) {
					.operator = t->kind, .type = ELEM_TYPE_OPERATOR,
				};
				stack_elem_push (operators, elem);
				did_unary = 1;
				break;
			case '+': // fall-through
			case '*': // fall-through
			case '/': // fall-through
			case '-': // fall-through
				{
					// This is a kludge, a hack. BC can do 2 - - - - - 10, so
					// this should be able to.
					if (did_unary) {
						ts_errno =
							ts_errno_maths_cant_have_minus_followed_by_minus;
						return r;
					}
					elem = (struct elem) {
						.operator = t->kind, .type = ELEM_TYPE_OPERATOR,
					};

					int top_precedence = operators->t? _ts_get_precedence
						(stack_elem_peek (operators).operator): 0;
					if (top_precedence) {
						int this_precedence = _ts_get_precedence
							(t->kind);
						while (operators->t && (_ts_get_precedence
								(stack_elem_peek (operators).operator) >
								this_precedence

								// This == 'u' is really checking to see if the
								// operator is left associative. There's
								// nothing left-associative here except u so
								// just check for u.

								|| (_ts_get_precedence
								(stack_elem_peek (operators).operator) ==
								this_precedence && t->kind == 'u'))) {
							stack_elem_push (&r, stack_elem_pop (operators));
						}
					}
					stack_elem_push (operators, elem);
				}
				break;
			case '(':
				elem = (struct elem) {
					.operator = t->kind, .type = ELEM_TYPE_OPERATOR,
				};
				stack_elem_push (operators, elem);
				break;
			case ')': // if the token is a right paren (i.e. ")"), then:
				if (!operators->t)
					assert (! "tmp, replace with return and ts_errno");
				// while the operator at the top of the operator stack is not
				// a left bracket
				while (stack_elem_peek (operators).operator != '(')
					stack_elem_push (&r, stack_elem_pop (operators));
				if (stack_elem_peek (operators).operator == '(')
					stack_elem_pop (operators);
				break;
			case KIND_ISO:
				elem = (struct elem) {
					.time = _time_from_iso_8601_str (t, now),
						.type = ELEM_TYPE_TIME
				};
				if (ts_errno)
					return r;
				stack_elem_push (&r, elem);
				did_unary = 0;
				expect_first = 0;
				break;
			case KIND_DURATION:
				if (*t->e == 'T') {
					// This is for, eg, 2dT17:00:00.
					struct ts_time time = _ts_get_time (e, t, now, TS_MATCH_ONLY_ISO);
					elem = (struct elem) {
						.time = time,
						.type = ELEM_TYPE_TIME
					};
					stack_elem_push (&r, elem);
					did_unary = 0;
					expect_first = 0;
					break;
				} else {
					elem = (struct elem) {
						.dur = ts_dur_from_dur_str (t->s, t->e),
							.type = ELEM_TYPE_DURATION,
					};
				}
				if (ts_errno)
					return r;
				_deal_with_unary_and_first (&elem, operators,
						expect_first, now);
				stack_elem_push (&r, elem);
				did_unary = 0;
				expect_first = 0;
				break;
			case KIND_CONSTANT:
				// Constants are immediately expanded into their values,
				// like, I think, macros.
				if (!dry_run) {
					if ((*t->constant).type == TS_CONSTANT_TYPE_TIME) {
						elem.time = (*t->constant).time;
						elem.type = ELEM_TYPE_TIME;
					} else if ((*t->constant).type == TS_CONSTANT_TYPE_DURATION) {
						elem.dur = (*t->constant).dur;
						elem.type = ELEM_TYPE_DURATION;
						_deal_with_unary_and_first (&elem, operators,
								expect_first, now);
					} else
						assert (0);
				}
				stack_elem_push (&r, elem);
				did_unary = 0;
				expect_first = 0;
				break;
			case KIND_DOT_DOT:
				if (!(allow & ALLOW_RANGE)) {
					ts_errno = ts_errno_bad_char;
					return r;
				} else
					*t = last_token;
				return r;
			case KIND_ERROR:
				ts_errno = ts_errno_bad_char;
				return r;
			case KIND_ERROR_UNRECOGNISED_CONSTANT:
				ts_errno = ts_errno_unrecognised_constant;
				return r;
				break;
			default:
				assert (0);
				break;
		}
		if (t->kind == '(' || t->kind == '+' || t->kind == '-'
				|| t->kind == '*' || t->kind == '*')
			expect_unary = 1;
		else
			expect_unary = 0;

		last_token = *t;
	}

	while (operators->t > 0) {
		elem = stack_elem_pop (operators);
		if (elem.operator == '(')
			ts_errno = ts_errno_maths_probably_unmatched_brackets;
		stack_elem_push (&r, elem);
	}
	return r;
}

// Adds up the postfixed queue.
struct ts_time _ts_do_sums (struct stack_elem *postfix, struct ts_time *now) {
	// This is the tm that you use for durs.

	struct stack_time_t new_stack = stack_time_t_create (8);

	for (size_t i = 0; i < postfix->t; i++) {
		if (postfix->d[i].type == ELEM_TYPE_OPERATOR) {
			char op = postfix->d[i].operator;
			if (!new_stack.t)
				assert (!"proper ts_errno here");
			time_t elems[2] = {
				stack_time_t_pop (&new_stack),
			};
			elems[1] = new_stack.t? stack_time_t_pop (&new_stack): 0;
			// Need to swap if dur's before absolute time because you'd
			// get < 0 and nonsense
			if (elems[0] < elems[1])
				SWAP (elems[0], elems[1]);

			switch (op) {
				case '+':
					stack_time_t_push (&new_stack, elems[0] + elems[1]);
					break;
				case '-':
					stack_time_t_push (&new_stack, elems[0] - elems[1]);
					break;
				case '*':
					stack_time_t_push (&new_stack, elems[0] * elems[1]);
					break;
				case '/':
					stack_time_t_push (&new_stack, elems[0] / elems[1]);
					break;
				case 'u':
					stack_time_t_push (&new_stack, -elems[0]);
					break;
				default:
					assert (!"Need ts_errno here");
					break;
			}
		} else
			stack_time_t_push (&new_stack,
					postfix->d[i].type == ELEM_TYPE_TIME
						? postfix->d[i].time.ts
						: ts_secs_from_dur (postfix->d[i].dur,
					now));
	}

	assert (new_stack.t == 1);
	struct ts_time r = ts_time_from_ts (*new_stack.d);
	free (new_stack.d);
	return r;
}

// Justification: this function could called ts_time_from_str twice,
// but I don't want to do that, since I'd have to add another argument
// to make it not error on '..'. So there's repetition.
struct ts_time_range ts_maths_time_range_from_str (
		const char *s, // Start of parsing.
		const char *e, // End of parsing. If 0, parse until '\0.

		struct ts_time *now,
		struct ts_constant *constants, size_t nconstants, bool dry_run) {

	struct ts_time_range r = {};
	str_s = s;

	if (!e)
		e = strchr (s, '\0');
	if (isspace (*(e - 1))) {
		e--;
		while (isspace (*e))
			e--;
		e++;
	}
	str_e = e;

	struct stack_elem operators = stack_elem_create (8);
	struct token t = {.e = s};
	struct stack_elem output_queue = _ts_get_postfix (&t, e, &operators,
			now, constants, nconstants, ALLOW_MATHS | ALLOW_CONSTANTS |
			ALLOW_RANGE, dry_run);

	if (ts_errno)
		goto error;
	r.s = _ts_do_sums (&output_queue, now);
	if (ts_errno)
		goto error;

	if (!*t.s) {
		ts_errno = ts_errno_range_expected_dot_dot;
		return r;
	}
	// ALLOW_CONSTANTS because a constant might be the end of the range.
	// It feels janky. Perhaps I'm just insecure.
	t = ts_get_token (t.e, e, ALLOW_RANGE | ALLOW_CONSTANTS, 0, 0);
	if (t.kind != KIND_DOT_DOT) {
		ts_errno = ts_errno_range_expected_dot_dot;
		return r;
	}

	free (output_queue.d);

	operators.t = 0;
	output_queue = _ts_get_postfix (&t, e, &operators, now, constants, nconstants,
			ALLOW_MATHS | ALLOW_CONSTANTS, dry_run);

	if (ts_errno)
		goto error;

	if (dry_run) {
		r = (struct ts_time_range) {};
		goto error;
	}
	r.e = _ts_do_sums (&output_queue, now);
	if (ts_errno)
		goto error;

error:
	if (output_queue.d)
		free (output_queue.d);
	if (operators.d)
		free (operators.d);
	return r;
}

struct ts_time ts_maths_time_from_str (const char *s, const char *e,
		struct ts_time *now, struct ts_constant
		*constants, size_t nconstants, bool dry_run) {

	str_s = s;
	struct ts_time r = {};
	if (!e)
		e = strchr (s, '\0');
	// Ignore trailing whitespace
	if (isspace (*(e - 1))) {
		e--;
		while (isspace (*e))
			e--;
		e++;
	}
	str_e = e;

	struct stack_elem operators = stack_elem_create (8);
	struct token t = {.e = s};
	struct stack_elem output_queue = _ts_get_postfix (&t, e, &operators, now,
			constants, nconstants, ALLOW_CONSTANTS, dry_run);
	if (ts_errno || dry_run)
		goto error;
	r = _ts_do_sums (&output_queue, now);
	if (ts_errno)
		goto error;

error:
	if (output_queue.d)
		free (output_queue.d);
	if (operators.d)
		free (operators.d);
	return r;

}

// Don't bother with ts_constants here. No use for it, right? Probably
// there is.
struct ts_time ts_time_from_str (const char *s, const char *e,
		struct ts_time *now, enum ts_match match) {
	// For printing
	str_s = s;
	if (!e)
		e = strchr (s, '\0');
	// Ignore trailing whitespace
	if (isspace (*(e - 1))) {
		e--;
		while (isspace (*e))
			e--;
		e++;
	}
	str_e = e;

	struct token t = ts_get_token (s, e, ALLOW_NOTHING, 0, 0);

	struct ts_time ts_time = {
		.ts = now->ts,
		.tm = now->tm,
	};
	// This is dodgy. Too subtle behaviour. This function should simple take a
	// struct time.
	//
	// And it does now (2021-05-12T12:41:10+01:00). But I'll leave this code here.
	if (!now->ts)
		localtime_r (&now->ts, &ts_time.tm);
	return _ts_get_time (e, &t, &ts_time, match);
}

struct ts_ts_range ts_maths_ts_range_from_str (const char *s,
		const char *e, struct ts_time *now, struct
		ts_constant *constants, size_t nconstants, bool dry_run) {

	struct ts_time_range ret = ts_maths_time_range_from_str (s, e, now,
			constants, nconstants, dry_run);
	return (struct ts_ts_range) {
		.s = ret.s.ts,
		.e = ret.e.ts
	};

}
// Parses an ISO date or a dur into a ts.
// On failure returns 0 and ts_errno is set.
// This could take now_secs since presumably you need to time (0) to
// get now_tm.
// If e, e is the end of the str, else it's assumed it's zero-terminated
// It accepts an INTERVAL or an iso_8601.
// This could take a struct ts_time but it doesn't in case you only have
// ts and don't want to make tm.
time_t ts_ts_from_str (
		const char *s, // Start of parsing.
		const char *e, // End of parsing. If 0, parse until '\0.
		struct ts_time *now,
		enum ts_match match) {
	return ts_time_from_str (s, e, now, match).ts;
}

// This function only exists so you can just
// 		struct tm tm = ts_tm_from_ts (&ts);
// 	I dunno if it's cause, cause you still have to check strerror.
struct tm ts_tm_from_ts (time_t ts) {
	// You don't have to set isdst here: localtime does that
	struct tm r = {};
	if (!localtime_r (&ts, &r))
		ts_errno = ts_errno_check_libc_errno;
	return r;
}

struct ts_time ts_time_from_ts (time_t ts) {
	// You don't have to set isdst here: localtime does that
	struct ts_time r = {.ts = ts};
	if (!localtime_r (&r.ts, &r.tm))
		ts_errno = ts_errno_check_libc_errno;
	return r;
}


// set_to_tm contains the values you want to set the members of the
// tm made from the ts to.
// The point of this function is to get the ts of eg 00:00:00
// tomorrow. You give it now + secs_from_days (2) and it'll round
// down.
int ts_tm_from_ts_set_units (time_t ts, struct tm set_to_tm, struct tm *out) {
	localtime_r (&ts, out);
	out->tm_sec = set_to_tm.tm_sec == -1
		? out->tm_sec
		: set_to_tm.tm_sec;
	out->tm_min = set_to_tm.tm_min == -1
		? out->tm_min
		: set_to_tm.tm_min;
	out->tm_hour = set_to_tm.tm_hour == -1
		? out->tm_hour
		: set_to_tm.tm_hour;
	out->tm_mday = set_to_tm.tm_mday == -1
		? out->tm_mday
		: set_to_tm.tm_mday;
	out->tm_mon = set_to_tm.tm_mon == -1
		? out->tm_mon
		: set_to_tm.tm_mon;
	out->tm_year = set_to_tm.tm_year == -1
		? out->tm_year
		: set_to_tm.tm_year;
	if (!ts_tm_is_valid (out))
		return 1;
	return 0;
}

time_t ts_ts_with_units_set (time_t ts, struct tm set_to_tm) {
	struct tm tm;
	if (ts_tm_from_ts_set_units (ts, set_to_tm, &tm))
		return -1;
	return mktime (&tm);
}

struct ts_time ts_time_with_units_set (time_t ts, struct tm set_to_tm) {

	// Returns .ts = -1 on error. I wouldn't do this now.

	struct ts_time r = {.ts = -1};
	if (ts_tm_from_ts_set_units (ts, set_to_tm, &r.tm))
		return r;
	r.ts = ts_ts_with_units_set (ts, set_to_tm);
	return r;
}

char *ts_str_from_time (char *buf, size_t n_buf, struct ts_time *time, char *fmt) {
	char *r = 0;
	bool buf_given = buf;
	if (!buf)
		assert ((buf = malloc (n_buf * sizeof *buf + 1)));
	size_t strftime_ret = strftime (buf, n_buf, fmt, &time->tm);
	if (strftime_ret == 0) {
		ts_errno = ts_errno_strftime_returned_0;
		goto fail;
	}
	r = buf;
	return r;
fail:
	if (!buf_given)
		free (buf);
	return 0;
}

// Return through parameter so you can provide buffer.
// Returns the end of the str strftime made
char *ts_str_from_ts (char *buf, size_t n_buf, time_t ts,
		char *fmt) {
	char *r = 0;
	bool buf_given = buf;
	if (!buf)
		assert ((buf = malloc (n_buf * sizeof *buf + 1)));
	struct ts_time time = ts_time_from_ts (ts);
	if (ts_errno)
		goto fail;
	r = ts_str_from_time (buf, n_buf, &time, fmt);
	if (ts_errno)
		goto fail;
	return r;
fail:
	if (!buf_given)
		free (buf);
	return 0;
}

// Returns the end of the str because strftime in ts_str_from_tm
// can fail, hence you can't really use without checking it.
char *ts_str_from_time_range (char *buf, size_t n_buf,
		struct ts_time_range *range, char *fmt, bool spaces) {

	// FIXME: there's some memory idiocy here, but I can't be bothered
	// to fix it.
	char *p = ts_str_from_time (buf, n_buf, &range->s, fmt);

	p = memchr (p, 0, n_buf);
	if ((p - buf) + (spaces? 4: 2) + (p - buf) >= n_buf)
		assert (!"You've gone over n_buf");

    if (spaces) p += sprintf (p, " .. ");
    else        p += sprintf (p, "..");

	ts_str_from_time (p, n_buf, &range->e, fmt);
	return memchr (p, 0, n_buf);

}

time_t ts_secs_from_dur (struct ts_dur dur, struct ts_time *time) {
	return ts_secs_from_months (dur.months, time) + dur.secs;
}



// Returns the _end of the str_.
// Function is repetitious, even with macro.
// You can pass 0 to *from_tm and it'll make one for you.
char *ts_str_from_dur (char *buf, size_t n_buf,
		struct ts_dur *dur,

		// This ts_time is only used for its tm. I've decided to make
		// you pass whole ts_time to all function, just to make things
		// more straightforward.
		struct ts_time *from_time,
		bool insert_spaces) {

	// If you don't pass a from_time, we just start from this very
	// moment.
	struct ts_time default_from_time = {};

	// Pointer trickiness: if you pass a null from_time, from_tm will
	// point to default_from_tm, which will be initialised to now.
	if (!from_time) {
		default_from_time.ts = time (0);
		assert (localtime_r (&default_from_time.ts, &default_from_time.tm));
		from_time = &default_from_time;
	}
	time_t years = 0;
	for (size_t month = 0; month < dur->months; month++) {
		if (month != 0 && month % 11 == 0)
			years++;
	}
	for (size_t year = 0; year < years; year++)
		dur->months -= 12;

	char *p = buf;
	int sprintf_ret = 0;
	if (years) {
		sprintf_ret = sprintf (ts_scratch_buffer, "%ldy", years);
		if ((!insert_spaces && p + sprintf_ret - buf >= n_buf) ||
					(insert_spaces && p + sprintf_ret + 1 - buf >= n_buf)) {
			ts_errno = ts_errno_buffer_overflow;
		}
		memcpy (p, ts_scratch_buffer, sprintf_ret);
		p += sprintf_ret;
		if (insert_spaces)
			*p++ = ' ';
	}
	if (dur->months) {
		sprintf_ret = sprintf (ts_scratch_buffer, "%zuM", dur->months);
		if ((!insert_spaces && p + sprintf_ret - buf >= n_buf) ||
					(insert_spaces && p + sprintf_ret + 1 - buf >= n_buf))
			ts_errno = ts_errno_buffer_overflow;

		memcpy (p, ts_scratch_buffer, sprintf_ret);
		p += sprintf_ret;
		if (insert_spaces) *p++ = ' ';
	}

	time_t months_secs = ts_secs_from_months (dur->months, from_time);
	time_t total_secs = dur->secs + months_secs;
	time_t remaining_secs = total_secs - months_secs;

	time_t val;

	bool is_first = 1;

#define $flip() ({ if (total_secs < 0) { if (!is_first) val = -val; is_first = 0; } })

// This feels ugly. ts_dur_str_from_dur could certainly
// be more ELEGANT. Update (2022-12-01T23:52:37+00:00): Yeah, it's
// pretty atrocious.
#define _TS_APP_TO_DUR_STR(x_from_secs, secs_from_x, unit_char) ({ \
	val = (x_from_secs (remaining_secs)); \
	remaining_secs -= (secs_from_x (val)); \
	if (val) { \
		$flip (); \
		sprintf_ret = sprintf (ts_scratch_buffer, "%ld%c", val, unit_char); \
		if ((!insert_spaces && p + sprintf_ret - buf >= n_buf) || \
					(insert_spaces && p + sprintf_ret + 1 - buf >= n_buf)) { \
			ts_errno = ts_errno_buffer_overflow; \
		} \
		memcpy (p, ts_scratch_buffer, sprintf_ret); \
		p += sprintf_ret; \
		if (insert_spaces) *p++ = ' '; \
	}  \
})

	_TS_APP_TO_DUR_STR (TS_YEARS_FROM_SECS, TS_SECS_FROM_YEARS, 'y');
	_TS_APP_TO_DUR_STR (TS_WEEKS_FROM_SECS, TS_SECS_FROM_WEEKS, 'w');
	_TS_APP_TO_DUR_STR (TS_DAYS_FROM_SECS, TS_SECS_FROM_DAYS, 'd');
	_TS_APP_TO_DUR_STR (TS_HOURS_FROM_SECS, TS_SECS_FROM_HOURS, 'h');
	_TS_APP_TO_DUR_STR (TS_MINS_FROM_SECS, TS_SECS_FROM_MINS, 'm');

	val = remaining_secs;
	$flip ();
	sprintf_ret = sprintf (ts_scratch_buffer, "%lds", val);
	memcpy (p, ts_scratch_buffer, sprintf_ret);
	p += sprintf_ret;

	if (insert_spaces)
		p--;
	*p = '\0';
	return buf;
}


void ts_add_dur_to_time_until (struct ts_time *ts_time,
		time_t now_ts, struct ts_dur *dur, bool less_than) {
	time_t secs;

	for (;;) {
		secs = ts_secs_from_dur (*dur, ts_time);

		if (ts_time->ts + (less_than? secs: 0) > now_ts)
			break;

		ts_time->ts += secs;
		localtime_r (&ts_time->ts, &ts_time->tm);
	}

}

void ts_subtract_dur_from_time_until (struct ts_time *ts_time,
		time_t now_ts, struct ts_dur *dur, bool less_than) {
	time_t secs;
	for (;;) {
		secs = ts_secs_from_dur (*dur, ts_time);
		if (ts_time->ts - (less_than? secs: 0) < now_ts)
			break;

		ts_time->ts += secs;
		localtime_r (&ts_time->ts, &ts_time->tm);
	}
}

// Not sure about this. Perhaps you could just call ts_update_time_by_dur_until
// twice. // But then maybe you'd end up with e < s.
void ts_add_dur_to_time_range_until (struct ts_time_range *ts_time,
		time_t now_ts, struct ts_dur *dur) {
	while (ts_time->s.ts < now_ts) {
		ts_time->s.ts += ts_secs_from_dur (
				*dur, &ts_time->s);
		ts_time->e.ts += ts_secs_from_dur (
				*dur, &ts_time->e);
		// This needs to be updated in case we go into the next month.
		// I could check to see we have in some other way. But I hope I never
		// am bored enough to do that.
		localtime_r (&ts_time->s.ts, &ts_time->s.tm);
	}
	localtime_r (&ts_time->s.ts, &ts_time->s.tm);
	localtime_r (&ts_time->e.ts, &ts_time->e.tm);
}

struct timespec double_to_struct_timespec (double the_double) {
	struct timespec r = {
		.tv_sec = (time_t)
			the_double,
	};
	r.tv_nsec = (long) (1000000000 * (the_double - r.tv_sec));
	return r;
}

struct ts_time add_dur_to_time (struct ts_time time,
		struct ts_dur dur) {

	return ts_time_from_ts (time.ts + (ts_secs_from_dur (dur, &time)));
}

struct ts_time subtract_dur_from_time (struct ts_time time, struct ts_time now,
		struct ts_dur dur) {

	return ts_time_from_ts (time.ts - (ts_secs_from_dur (dur, &now)));
}

struct ts_time_range ts_range_plus_dur (struct ts_time_range *range,
		struct ts_dur *dur) {

	range->s = ts_time_from_ts (range->s.ts
		+ (ts_secs_from_dur (*dur, &range->s)));
	range->e = ts_time_from_ts (range->e.ts
		+ (ts_secs_from_dur (*dur, &range->e)));
	return *range;
}

char *ts_strerror (enum ts_errno ts_errno) {
	if (error_token.e)
		_ts_back_up_over_spaces (&error_token.e);
	const char *p = str_s;
	char *q = ts_scratch_buffer;
	bool colour_error_messages = 1;
#ifdef TS_NO_COLOUR_ERROR_MESSAGES
	colour_error_messages = 0;
#endif
	bool colour_open = 0;
	while (*p && p < str_e) {
		// A poor hack. error_token doesn't get set if the error
		// happens after lexing.
		if (error_token.e) {
			if (p - str_s == error_token.s - str_s) {
				if (colour_error_messages) {
					memcpy (q, ANSI_RED, sizeof ANSI_RED);
					q += sizeof ANSI_RED - 1;
					colour_open = 1;
				} else {
					*q++ = '>';
					*q++ = '>';
				}
			} else if (p - str_s == error_token.e - str_s) {
				if (colour_error_messages) {
					memcpy (q, ANSI_NO_COLOUR, sizeof ANSI_NO_COLOUR);
					q += sizeof ANSI_NO_COLOUR - 1;
					colour_open = 0;
				} else {
					*q++ = '<';
					*q++ = '<';
				}
			}
		}
		*q++ = *p++;
	}
	if (colour_open) {
		memcpy (q, ANSI_NO_COLOUR, sizeof ANSI_NO_COLOUR);
		q += sizeof ANSI_NO_COLOUR - 1;
	}
	*q = '\0';

	sprintf (q + 1, "%s: \"%.*s\"", ts_errno_strs[ts_errno],
			(int) strlen (ts_scratch_buffer), ts_scratch_buffer);

#if 0
	sprintf (ts_scratch_buffer, ts_errno_strs[ts_errno],
			(int) (error_token.e - error_token.s), error_token.s);
#endif
	return q + 1;
	/* return ts_errno_strs[ts_errno]; */
}


