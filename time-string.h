#pragma once
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

/*
 * A time_string is either an DURATION, like 2d5m2s, or an absolute
 * ISO time like 1990-09-09T01:01:01.
 * DURATION STRINGS: consists of a number followed by a unit.
 * 		UNITS: s (secs), m (mins), d (days), w (weeks), M (months)
 * 				and y (years).
 * 			They are case sensitive: m is minutes and M is months.
 * 		Spaces are allowed between each bit, like 2d 5m 2s.
 * ISO TIMESTAMPS: an ISO timestamp. "timestamp is always spelt" "ts"
 * 			in this lib. That's because I've used the lib for a while
 * 			and typed out all these long function names too many times.
 * 		You can use a space instead of a T.
 * 		No other spaces are allowed, except at the start and end of the
 * 				string.
 * 			"   1990-05" is fine, but "1990 - 05" isn't.
 * 		Years can be four digits (2009) or two (09).
 * 		All values except years can be one or two digits.
 * 			Eg: 1990-1-1 is the same as 1990-01-01.
 * 		Any part of the string after the year can be left off:
 * 			1990-05-03 = 1990-05-03T00:00:00.
 * 			1990 = 1990-01-01T00:00:00.
 * 		Stuff like 1990- or 1990-04-04T are errors.
 *
 * 	Structs:
 * 		There two main structs, ts_dur and ts_time.
 *
 * 		ts_time is a timestamp and and a struct tm.
 * 			The reason you want both is if you're working with durs,
 * 					at which point you want to know watch month it is.
 * 		ts_dur
 * 			Is a timestamp and a number of month. The total seconds is
 * 					got from ts_secs_from_dur.
 *
 * This library has two important functions
 * 		ts_time_from_str
 * 			which takes in
 * 				an ISO DATE
 * 				or an DURATION STRING
 * 			and returns a struct ts_time.
 * 		ts_dur_from_dur_str
 * 			which takes in
 * 				a str
 * 			and returns a struct dur.
 *
 * The rest of the function are variations on this theme. The range functions
 * 		return "ranges" -- struct with a start and an end.
 *
 * Most functions
 */

// There doesn't appear to be a fmt specifier for a full ISO 8601 date
#define TS_ISO_DATE_FMT "%Y-%m-%dT%H:%M:%S"
#define TS_ISO_DATE_TIME_ONLY "%H:%M:%S"

/* #define COLOUR_ERROR_MESSAGES 1 */
#define DURATION_CHRS "smhdwMy"

#define TS_SECS_FROM_YEARS(x) ((time_t) ((x) * 60.0 * 60.0 * 24.0 * 365.0))
#define TS_SECS_FROM_WEEKS(x) ((time_t) ((x) * 60.0 * 60.0 * 24.0 * 7.0))
#define TS_SECS_FROM_DAYS(x) ((time_t) ((x) * 60.0 * 60.0 * 24.0))
#define TS_SECS_FROM_HOURS(x) ((time_t) ((x) * 60.0 * 60.0))
#define TS_SECS_FROM_MINS(x) ((time_t) ((x) * 60.0))

#define TS_YEARS_FROM_SECS(x) ((time_t) ((x) / 60.0 / 60.0 / 24.0 / 365.0))
#define TS_WEEKS_FROM_SECS(x) ((time_t) ((x) / 60.0 / 60.0 / 24.0 / 7.0))
#define TS_DAYS_FROM_SECS(x) ((time_t) ((x) / 60.0 / 60.0 / 24.0))
#define TS_HOURS_FROM_SECS(x) ((time_t) ((x) / 60.0 / 60.0))
#define TS_MINS_FROM_SECS(x) ((time_t) ((x) / 60.0))

// Calling it "MDAY" because struct tm does.
#define TS_MDAY_JAN 31
#define TS_MDAY_FEB_NON_LEAP_YEAR 28
#define TS_MDAY_FEB_LEAP_YEAR 29
#define TS_MDAY_MAR 31
#define TS_MDAY_APR 30
#define TS_MDAY_MAY 31
#define TS_MDAY_JUN 30
#define TS_MDAY_JUL 31
#define TS_MDAY_AUG 31
#define TS_MDAY_SEP 30
#define TS_MDAY_OCT 31
#define TS_MDAY_NOV 30
#define TS_MDAY_DEC 31


// Total is months * (number of seconds in a month* + secs.
// Usually you don't need to use this. I've had to use it so I can
// write back an dur string. If you didn't have something like
// you'd lose the months, and you need the month to properly calculate
// a timestamp in your dur string contains months.
typedef struct ts_dur ts_dur;
struct ts_dur {
	ssize_t months;
	time_t secs;
};

typedef struct ts_dur_range ts_dur_range;
struct ts_dur_range {
	struct ts_dur s, e;
};

typedef struct ts_ts_range ts_ts_range;
struct ts_ts_range {
	time_t s, e;
};

// Often you want both. You need tm for calculating durs. Or
// at least you need the month and you get that from tm.
typedef struct ts_time ts_time;
struct ts_time {
	time_t ts; // timestamp
	struct tm tm;
};

enum ts_constant_type {
	TS_CONSTANT_TYPE_TIME,
	TS_CONSTANT_TYPE_DURATION,
	TS_CONSTANT_TYPE_TIME_RANGE,
};
typedef enum ts_constant_type ts_constant_type;

typedef struct ts_time_range ts_time_range;
struct ts_time_range {
	struct ts_time s, e;
};

typedef struct ts_constant ts_constant;
struct ts_constant {
	char *name;
	// put in len field, because of strncmp in get_token_2?
	union {
		struct ts_time time;
		struct ts_time_range time_range;
		struct ts_dur dur;
	};
	enum ts_constant_type type;
};

#define TS_ERRNO \
	x(ts_errno_no_error, "No error") \
	x(ts_errno_lex_expected_dot_dot, "Expected \"..\"") \
	x(ts_errno_lex_str_isnt_dur_or_iso, "String doesn't resemble a dur or an ISO time") \
	x(ts_errno_lex_dur_missing_unit, "Encountered number with no unit -- s|m|h|d|w|M|y after it") \
	x(ts_errno_lex_dur_expected_dur_chrs, "Encountered number with no unit -- s|m|h|d|w|M|y after it") \
	x(ts_errno_lex_iso_was_T_but_nothing_after, "Encountered T but nothing after") \
	x(ts_errno_lex_iso_expected_digit, "Expected digit") \
	x(ts_errno_lex_iso_expected_0, "Expected end of string") \
	x(ts_errno_lex_apparently_iso_time_began_with_minus, "What looks like an ISO time began with minus") \
	x(ts_errno_lex_iso_wrong_number_of_year_digits, "Encountered ISO year with number of digits other than two or four") \
	x(ts_errno_lex_iso_wrong_number_of_month_digits, "Encountered month with number of digits other than two") \
	x(ts_errno_lex_iso_wrong_number_of_day_digits, "Encountered day with number of digits other than two") \
	x(ts_errno_lex_iso_wrong_number_of_hour_digits, "Encountered hour with number of digits other than two") \
	x(ts_errno_lex_iso_wrong_number_of_minute_digits, "Encountered minute with number of digits other than two") \
	x(ts_errno_lex_iso_wrong_number_of_second_digits, "Encountered seconds with number of digits other than two") \
	x(ts_errno_lex_iso_expected_hyphen, "Expected hyphen") \
	x(ts_errno_lex_iso_expected_colon, "Expected colon") \
	x(ts_errno_lex_iso_expected_T, "Expected 'T' or ' '") \
	x(ts_errno_lex_passed_NULL_constants, "Found apparent constant but constants is NULL") \
	x(ts_errno_lex_nconstants_is_0, "Found apparent constant but nconstants is 0") \
	x(ts_errno_parse_dur_too_big_digit, "Digit is too big") \
	x(ts_errno_parse_dur_expected_digit, "Expected digit") \
	x(ts_errno_parse_iso_month_not_plusequals_zero, "Month must be 0 minimum") \
	x(ts_errno_parse_iso_mday_not_plusequals_one, "Day of must be 1 minimum") \
	x(ts_errno_parse_iso_hour_not_plusequals_zero, "Hours must be 0 minimum") \
	x(ts_errno_parse_iso_min_not_plusequals_zero, "Minutes must be 0 minimum") \
	x(ts_errno_parse_iso_sec_not_plusequals_zero, "Seconds must be 0 minimum") \
	x(ts_errno_parse_iso_too_big_mon, "Too big number of months in the year") \
	x(ts_errno_parse_iso_too_big_mday, "Too big number of days in the month") \
	x(ts_errno_parse_iso_too_big_hour, "Too big number of hours") \
	x(ts_errno_parse_iso_too_big_min, "Too big number of minutes") \
	x(ts_errno_parse_iso_too_big_sec, "Too big number of seconds") \
	x(ts_errno_passed_match_only_dur_but_str_is_iso, "Passed TS_MATCH_ONLY_DURATION but the string is iso") \
	x(ts_errno_passed_match_only_iso_but_str_is_dur, "Passed TS_MATCH_ONLY_ISO but the string is dur") \
	x(ts_errno_asked_for_dur_but_asked_for_only_iso, "Asked for dur and dur but also asked for ISO") \
	x(ts_errno_not_an_dur_str, "Not a dur string") \
	x(ts_errno_strftime_returned_0, "Strftime returned 0") \
	x(ts_errno_range_must_have_two_times, "Range needs a \"..\" in the middle") \
	x(ts_errno_range_expected_dot_dot, "Expected \"..\"") \
	x(ts_errno_lex_range_dot_dot_should_be_digit_after, "Expected digit after \"..\"") \
	x(ts_errno_malloc_failed, "Malloc failed") \
	x(ts_errno_buffer_overflow, "A buffer overflowed") \
	x(ts_errno_check_libc_errno, "Check libc errno. This error message is dumb. The libc message should be in this") \
	x(ts_errno_maths_cant_have_minus_followed_by_minus, "Can't do minuses followed by minus unless it's unary. This is a bug") \
	x(ts_errno_bad_char, "Unrecognised char") \
	x(ts_errno_unrecognised_constant, "Undefined constant") \
	x(ts_errno_maths_probably_unmatched_brackets, "Probably unmatch brackets")

// ts: time_string
// ts_errno is zeroed at the start of get_token
#define x(a, b) a,
extern enum ts_errno {TS_ERRNO} ts_errno;
#undef x

// return_timestamp is 0 because that's the default

enum ts_match {
	TS_MATCH_ALL,
	TS_MATCH_ONLY_DURATION,
	TS_MATCH_ONLY_ISO,
};
typedef enum ts_match ts_match;

struct ts_time ts_time_from_str (const char *s, const char *e,
		struct ts_time *now, enum ts_match only);

struct ts_time ts_maths_time_from_str (const char *s, const char *e,
		struct ts_time *now, struct ts_constant *constants,
		size_t nconstants, bool dry_run);

struct ts_time_range ts_maths_time_range_from_str (const char *s, const
		char *e, struct ts_time *now, struct ts_constant
		*constants, size_t nconstants, bool dry_run);

struct ts_ts_range ts_maths_ts_range_from_str (const char *s,
		const char *e, struct ts_time *now, struct
		ts_constant *constants, size_t nconstants, bool dry_run);

time_t ts_ts_from_str (const char *s, const char *e,
		struct ts_time *now, enum ts_match only);

struct ts_time_range ts_time_range_from_str (const char *s, const char *e,
		struct ts_time *now, enum ts_match match);

struct ts_ts_range ts_ts_range_from_str (const char *s,
		const char *e, struct ts_time *now,
		enum ts_match match);

// Returns an dur, eg 5s
#if 0
time_t ts_get_secs_from_dur_str (const char *s, const char *e, time_t from);
// Returns a _struct_ dur
#endif

struct ts_dur ts_dur_from_dur_str (const char *s,
		const char *e);

struct ts_dur_range ts_dur_range_from_dur_str (const char *s,
		const char *e, bool reorder);

// Conversion functions
struct tm ts_tm_from_ts (time_t ts);

struct ts_time ts_time_from_ts (time_t ts);

int ts_tm_from_ts_set_units (time_t ts, struct tm set_to_tm, struct tm *out);

struct ts_time ts_time_with_units_set (time_t ts, struct tm set_to_tm);

time_t ts_ts_with_units_set (time_t ts, struct tm set_to_tm);

char *ts_str_from_ts (char *buf, size_t nbuf, time_t ts, char *fmt);

char *ts_str_from_time (char *buf, size_t nbuf, struct ts_time *time, char *fmt);

char *ts_str_from_time_range (char *buf, size_t n_buf,
		struct ts_time_range *range, char *fmt, bool spaces);

char *ts_str_from_dur (char *res, size_t nres, struct ts_dur *dur,
		struct ts_time *from_time, bool insert_spaces);

time_t ts_secs_from_dur (struct ts_dur dur, struct ts_time *time);

struct ts_time add_dur_to_time (struct ts_time time, struct ts_dur dur);

struct ts_time subtract_dur_from_time (struct ts_time time, struct ts_time now,
		struct ts_dur dur);

void ts_add_dur_to_time_until (struct ts_time *time, time_t now_ts,
		struct ts_dur *dur, bool less_than);

void ts_subtract_dur_from_time_until (struct ts_time *ts_time,
		time_t now_ts, struct ts_dur *dur, bool less_than);

void ts_add_dur_to_time_range_until (struct ts_time_range *ts_time,
		time_t now_ts, struct ts_dur *dur);

struct ts_time_range ts_range_plus_dur (struct ts_time_range *range,
		struct ts_dur *dur);

// Convenience
bool ts_is_leap_year (struct ts_time *time);
bool ts_tm_is_valid (struct tm *tm);
time_t ts_secs_from_months (int months, struct ts_time *from_time);
struct timespec double_to_struct_timespec(double the_double);
// Error
char *ts_strerror (enum ts_errno ts_errno);
