# Time String

This is a time-and-date parsing library. It parses [ISO 8601
dates/times](https://en.wikipedia.org/wiki/ISO_8601). It also parses
durations (like 4 minutes), and intervals (like 19-09-09 to 19-09-10).
It also does maths on dates.

You can just include it in your project in the usual way or install it
as a dynamic library using meson.

To do that, get meson if you don't  have it, then:

```sh

meson build
ninja -Cbuild install

```

You might have to put sudo in front of the "ninja".
